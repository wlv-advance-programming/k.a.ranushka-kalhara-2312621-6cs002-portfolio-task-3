package src.objects;

import java.awt.Color;
import java.awt.Graphics;

public class DrawDigitGivenCentreParameterSet {
	public Graphics g;
	public int x;
	public int y;
	public int diameter;
	public int n;
	public Color c;

	public DrawDigitGivenCentreParameterSet(Graphics g, int x, int y, int diameter, int n, Color c) {
		this.g = g;
		this.x = x;
		this.y = y;
		this.diameter = diameter;
		this.n = n;
		this.c = c;
	}
}