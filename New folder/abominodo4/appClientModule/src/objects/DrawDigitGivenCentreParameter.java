package src.objects;

import java.awt.Graphics;

public class DrawDigitGivenCentreParameter {
	public Graphics g;
	public int x;
	public int y;
	public int diameter;
	public int n;

	public DrawDigitGivenCentreParameter(Graphics g, int x, int y, int diameter, int n) {
		this.g = g;
		this.x = x;
		this.y = y;
		this.diameter = diameter;
		this.n = n;
	}
}