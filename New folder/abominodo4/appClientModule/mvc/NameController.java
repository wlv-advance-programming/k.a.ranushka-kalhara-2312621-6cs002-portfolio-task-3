package mvc;

import java.awt.event.*;

import src.Aardvark;

public class NameController {
    private NameModel model;
    private NameView view;

    public NameController(NameModel model, NameView view) {
        this.model = model;
        this.view = view;

        view.getSubmitButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = view.getName();
                model.setName(name);
            }
        });
    }

	public String getName() {
		return model.getName();
	}  
	
	public void setName(String name) {
		model.setName(name);
	}  
}
