package commandPattern;

import src.Aardvark;
import src.Domino;

public class AddPlacement implements Command {
	public int hx;
	public int hy;
	public int lx;
	public int ly;
	private Domino d;
	
	Aardvark ad = new Aardvark();
	
	public AddPlacement(int hx, int hy, int lx, int ly, Domino d) {
	    this.hx = hx;
	    this.hy = hy;
	    this.lx = lx;
	    this.ly = ly;
	    this.d = d;
	}
	
    @Override
    public void execute() {
    	d.place(this.hx, this.hy, this.lx, this.ly); 
    }
    
	@Override
	public void undo() {
		ad.undo(d);
	}
}
