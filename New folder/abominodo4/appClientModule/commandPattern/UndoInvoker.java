package commandPattern;

import java.util.Stack;

public class UndoInvoker {
	private Stack<Command> undoStack = new Stack<>();
	
    public void place(Command command) {
        command.execute();
        undoStack.push(command);
    }
	
	 public void undo() {
        if (!undoStack.empty()) {
            Command command = undoStack.pop();
            command.undo();
        }
	 }
}
