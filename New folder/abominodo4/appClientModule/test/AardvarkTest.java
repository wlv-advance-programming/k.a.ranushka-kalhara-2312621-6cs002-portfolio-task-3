package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import src.Aardvark;

class AardvarkTest {

	//Test case for duplicate code refactoring
	@Test
	void guessGrid() {
		Aardvark ad = new Aardvark();
		int[][] testDataSet = {
			{1,2,3,4,5,6,7,8},
			{1,2,3,4,5,6,7,8},
			{1,2,3,4,5,6,7,8},
			{1,2,3,4,5,6,7,8},
			{1,2,3,4,5,6,7,8},
			{1,2,3,4,5,6,7,8},
			{1,2,3,4,5,6,7,8},
		};
		int output = ad.guessGrid(testDataSet);
		assertEquals(11, output);
	}

}
