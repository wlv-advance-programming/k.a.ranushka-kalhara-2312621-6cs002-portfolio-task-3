package strategypattern;

import src.Aardvark;

public class SimplesDifficulty implements DifficultyStrategy {
	
  @Override
   public void play() {
	   Aardvark ad = new Aardvark();
       ad.notSoSimpleMode();
    }
}
