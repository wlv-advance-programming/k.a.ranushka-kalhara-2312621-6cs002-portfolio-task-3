package strategypattern;

import src.Aardvark;

public class SuperduperShuffledDifficulty implements DifficultyStrategy {
	
  @Override
   public void play() {
	   Aardvark ad = new Aardvark();
       ad.superDuperShuffledMode();
    }
}
