package strategypattern;

import src.Aardvark;

public class NotSoSimpleDifficulty implements DifficultyStrategy {
	
   @Override
   public void play() {
       Aardvark ad = new Aardvark();
       ad.simpleMode();
    }
}
