package strategypattern;

public interface DifficultyStrategy {
	void play();
}
