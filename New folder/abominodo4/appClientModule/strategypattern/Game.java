package strategypattern;

public class Game {
	DifficultyStrategy strategy;

    public void setDifficulty(DifficultyStrategy s) {
        strategy = s;
    }

    public void play() {
        strategy.play();
    }
}
